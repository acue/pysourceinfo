pysourceinfo
============

The 'pysourceinfo' package provides basic runtime information on executed 
sourcefiles based on 'inspect', 'sys', 'os', and additional sources.
The covered objects include packages, modules/files and functions/methods/scripts. 

The supported platforms are:

* Linux, BSD, Unix, OS-X, Cygwin, and Windows

* Python2.7+, Python3.5+


**Online documentation**:

* https://pysourceinfo.sourceforge.io/

**Runtime-Repository**:

* PyPI: https://pypi.org/project/pysourceinfo/

  Install: *pip install pysourceinfo*, see also 'Install'.


**Downloads**:

* sourceforge.net: https://sourceforge.net/projects/pysourceinfo/files/

* bitbucket.org: https://bitbucket.org/acue/pysourceinfo

* github.com: https://github.com/ArnoCan/pysourceinfo/

* pypi.org: https://pypi.org/project/pysourceinfo/
 

Project Data
------------

* PROJECT: 'pysourceinfo'

* MISSION: Support easy access to RTTI on Python source and binary files.

* VERSION: 00.01

* RELEASE: 00.01.032

* STATUS: alpha

* AUTHOR: Arno-Can Uestuensoez

* COPYRIGHT: Copyright (C) 2010,2011,2015-2017 Arno-Can Uestuensoez @Ingenieurbuero Arno-Can Uestuensoez

* LICENSE: Artistic-License-2.0 + Forced-Fairplay-Constraints


Python support: 

* Standard Python(CPython) - Python2.7, and Python3.5+ 

* PyPy - 5.10+ - Python2.7+, and Python3.5+

OS-Support:

* Linux: Fedora, CentOS, Debian, and Raspbian 

* BSD: OpenBSD, FreeBSD

* OS-X: Snow Leopard

* Windows: Win7, Win10

* Cygwin

* UNIX: Solaris


**Current Release**

Major Changes:

* Python2.6 support dropped.

* Python3.5+ support introduced.

* PyPy tests added.

* Changed module structure and fitting names into overall category based naming schema.

* Split more stack related parts into package *PyStackInfo*

* Added special support for decorators.

* Added support for some special cases of Python syntax elements, e.g. support of nested-classes.

* Added several new functions.

* Enhanced documentation

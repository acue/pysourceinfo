External References
===================

.. [functools2] functools/Python2.7+; 
   Python.org;
   `functools/Python2.7+ <https://docs.python.org/2/library/functools.html>`_

.. [functools3] functools/Python3.5+; 
   Python.org;
   `functools/Python3.5+ <https://docs.python.org/3.5/library/functools.html>`_

.. [inspect2] inspect/Python2.7+; 
   Python.org;
   `inspect/Python2.7+ <https://docs.python.org/2/library/inspect.html>`_

.. [inspect3] inspect/Python3.5+; 
   Python.org;
   `inspect/Python3.5+ <https://docs.python.org/3.5/library/inspect.html>`_

.. [msimio] Decorator module;
   Michele Simionato;
   2016;
   documents:`pypi.python.org/pypi/decorator <https://pypi.python.org/pypi/decorator>`_
   source:`decorator.readthedocs.io/en/stable/ <https://decorator.readthedocs.io/en/stable/>`_

.. [PEP273] Import Modules from Zip Archives;
   jim at interet.com (James C. Ahlstrom);
   11-Oct-2001;
   Python-Version:2.3+
   `Python.org <https://www.python.org/dev/peps/pep-0273/>`_;

.. [PEP302] New Import Hooks;
   Just van Rossum <just at letterror.com>, Paul Moore <p.f.moore at gmail.com>;
   19-Dec-2002;
   Python-Version:2.3+
   `Python.org <https://www.python.org/dev/peps/pep-0273/>`_;

.. [PEP3147] PYC Repository Directories;
   Barry Warsaw <barry at python.org>;
   2009-12-16;
   Python-Version:3.2+
   `Python.org <https://www.python.org/dev/peps/pep-3147/>`_

.. [PEP3155] Qualified name for classes and functions;
   Antoine Pitrou <solipsis at pitrou.net>;
   2011-10-29;
   Python-Version:3.3+
   `Python.org <https://www.python.org/dev/peps/pep-3155/>`_;

.. [PEP420] Implicit Namespace Packages;
   Eric V. Smith <eric at trueblade.com>;
   19-Apr-2012;
   Python-Version:3.3+
   `Python.org <https://www.python.org/dev/peps/pep-0420/>`_;

.. [PEP441] Improving Python ZIP Application Support;
   Eric V. Smith <eric at trueblade.com>;
   30 March 2013;
   Python-Version:3.3+
   `Python.org <https://www.python.org/dev/peps/pep-0441/>`_;

.. [pysourceinfo] pysourceinfo; 
   Arno-Can Uestuensoez @Ingenieurbuero Arno-Can Uestuensoez;
   2010,2011,2015-2017;
   `PyPI <https://pypi.python.org/pypi/pysourceinfo/>`_;
   `GitHub <https://github.com/ArnoCan/pysourceinfo/>`_;
   `docs @pythonhosted.org/pysourceinfo <https://pythonhosted.org/pysourceinfo/>`_

.. [pystackinfo] pystackinfo; 
   Arno-Can Uestuensoez @Ingenieurbuero Arno-Can Uestuensoez;
   August-2017;
   `PyPI <https://pypi.python.org/pypi/pystackinfo/>`_;
   `GitHub <https://github.com/ArnoCan/pystackinfo/>`_;
   `docs @pythonhosted.org/pystackinfo <https://pythonhosted.org/pystackinfo/>`_

.. [qualname] __qualname__ emulation for older Python versions;
   Wouter Bolsterlee;
   2015-04-11;
   `qualname <https://pypi.python.org/pypi/qualname/>`_

.. [types2] types/Python2.7+; 
   Python.org;
   `types/Python2.7+ <https://docs.python.org/2/library/types.html>`_

.. [types3] types/Python3.5+; 
   Python.org;
   `types/Python3.5+ <https://docs.python.org/3.5/library/types.html>`_

.. [xkcd] Profile Info;
   Randall Munroe;
   `xkcd.com <http://xkcd.com>`_ ;
   `Profile Info <http://xkcd.com/1303/>`_


ToDo
====

The API relevant significant todos:

#. PEP420 - Implicit Namespace Packages

   Understand `PEP 420 <https://www.python.org/dev/peps/pep-420/>`_, test, and fix if required. 

#. Check for Python3 the precedence of loaded module when source and compiled(pyc/pyo) are present 

#. Optimize code, when mostly any special case seems to be recognized and considered.
   Set appropriate version-schema than.


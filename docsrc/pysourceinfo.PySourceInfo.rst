'pysourceinfo.PySourceInfo' - Module
************************************

This module is deprecated in favor of 

* pysourceinfo.fileinfo
* pysourceinfo.bininfo
* pysourceinfo.helper
* pysourceinfo.infolists
* pysourceinfo.objectinfo

